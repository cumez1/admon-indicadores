<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function(){
    $test = App\Models\Ciudad::all();
    dd($test);
});



Route::get('/', function () {
    return view('index');
})->name('inicio');

Route::get('quienes-somos', function(){ return view('quienes-somos'); })->name('quienes.somos');
Route::get('equipo', function(){ return view('equipo'); })->name('equipo');
Route::get('contacto', function(){ return view('contacto'); })->name('contacto');

Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');
Route::get('dashboard-listado', 'DashboardController@listado')->name('dashboard.listado');
Route::get('indicadores-ventas', 'DashboardController@ventas')->name('dashboard.ventas');
Route::get('indicadores-productos', 'DashboardController@productos')->name('dashboard.productos');
Route::get('indicadores-clientes', 'DashboardController@clientes')->name('dashboard.clientes');

Route::group(['prefix' => 'indicadores'], function(){
    

    Route::get('productos-vendidos','IndicadorUnoController@index')->name('indicador.productos.vendidos');
    Route::post('listado', 'IndicadorUnoController@listado')->name('indicador.productos.vendido.listado');

    Route::get('ventas-mes','IndicadorDosController@index')->name('indicador.ventas.mes');
    Route::post('listado-ventas-mes', 'IndicadorDosController@listado')->name('indicador.ventas.mes.listado');

    Route::get('clientes-frecuentes','IndicadorTresController@index')->name('indicador.clientes.frecuentes');
    Route::post('listado-clientes-frecuentes', 'IndicadorTresController@listado')->name('indicador.clientes.frecuentes.listado');

    Route::get('comparacion-ventas','IndicadorCuatroController@index')->name('indicador.comparacion.ventas');
    Route::post('listado-comparacion-ventas', 'IndicadorCuatroController@listado')->name('indicador.comparacion.ventas.listado');
 	
 	Route::get('ciudad-mayor','IndicadorCincoController@index')->name('indicador.ciudad.mayor');
    Route::post('listado-ciudad-mayor', 'IndicadorCincoController@listado')->name('indicador.ciudad.mayor.listado');

    Route::get('productos-vendidos-estado','IndicadorSeisController@index')->name('indicador.vendidos.estado');
    Route::post('listado-productos-vendidos-estado', 'IndicadorSeisController@listado')->name('indicador.vendidos.estado.listado');
    
    Route::get('clientes-frecuentes-estado','IndicadorSieteController@index')->name('indicador.frecuentes.estado');
    Route::post('listado-clientes-frecuentes-estado', 'IndicadorSieteController@listado')->name('indicador.frecuentes.estado.listado');
    Route::post('buscar-ciudad', 'IndicadorSieteController@ciudad')->name('indicador.buscar.ciudad');

    Route::get('menos-vendido','IndicadorOchoController@index')->name('indicador.menos.vendido');
    Route::post('listado-menos-vendido', 'IndicadorOchoController@listado')->name('indicador.menos.vendido.listado');

    Route::get('stock-producto','IndicadorNueveController@index')->name('indicador.stock.producto');
    Route::post('listado-stock-producto', 'IndicadorNueveController@listado')->name('indicador.stock.producto.listado');

    Route::get('ventas-empleado','IndicadorDiezController@index')->name('indicador.ventas.empleado');
    Route::post('listado-ventas-empleado', 'IndicadorDiezController@listado')->name('indicador.ventas.empleado.listado');

	// indicador once 
    Route::get('mas-vendidos','IndicadorOnceController@index')->name('indicador.mas.vendidos');
    Route::post('listado-mas-vendidos', 'IndicadorOnceController@listado')->name('indicador.mas.vendidos.listado');

    // indicador doce
    Route::get('montos-totales','IndicadorDoceController@index')->name('indicador.montos.totales');
    Route::post('listado-montos-totales', 'IndicadorDoceController@listado')->name('indicador.montos.totales.listado');


    // indicador trece
    Route::get('categoria-favorita','IndicadorTreceController@index')->name('indicador.categoria.favorita');
    Route::post('listado-categoria-favorita', 'IndicadorTreceController@listado')->name('indicador.categoria.favorita.listado');


     // indicador catorce
    Route::get('devolucion-productos','IndicadorCatorceController@index')->name('indicador.devolucion.productos');
    Route::post('listado-devolucion-productos', 'IndicadorCatorceController@listado')->name('indicador.devolucion.productos.listado');


    // indicador Quince
    Route::get('cantidad-devuelto','IndicadorQuinceController@index')->name('indicador.cantidad.devuelto');
    Route::post('listado-cantidad-devuelto', 'IndicadorQuinceController@listado')->name('indicador.cantidad.devuelto.listado');

});
    