﻿<div class="classynav">
    <ul id="nav">

        <li ><a href="{{route('inicio')}}">Inicio</a></li>
        <li ><a href="{{route('dashboard.index')}}">Dashboard</a></li>
        <li><a href="#">Indicadores</a>
            <ul class="dropdown" >
                <li >
                    <a  href="{{route('indicador.productos.vendidos')}}">
                        - Ranking de productos más vendidos
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.ventas.mes')}}">
                        - Ranking ventas al mes </a>
                </li>
                <li>
                    <a  href="{{route('indicador.clientes.frecuentes')}}">
                        - Ranking clientes frecuentes </a> 
                </li>
                <li>
                    <a  href="{{route('indicador.comparacion.ventas')}}">
                        - Comparaciones de ventas por año </a>
                </li>
                
                <li>
                    <a  href="{{route('indicador.ciudad.mayor')}}">
                        - Ciudadades con mayor consumo de ... </a>
                </li>

                <li >
                    <a  href="{{route('indicador.vendidos.estado')}}">
                        - Tipo de productos mas vendidos por estado
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.frecuentes.estado')}}">
                        - Clientes más frecuentes por ciudad
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.menos.vendido')}}">
                        - Productos menos vendidos
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.stock.producto')}}">
                        - Stock de productos por rango de fechas </a>
                </li>
                
                <li>
                    <a  href="{{route('indicador.ventas.empleado')}}">
                        - Rankin de empleados que mas han vendido
                    </a>
                </li>
                
                 <li>
                    <a  href="{{route('indicador.mas.vendidos')}}">
                        - Producto más vendido
                    </a>
                </li>

                <li>
                    <a  href="{{route('indicador.montos.totales')}}">
                        - Monto total de productos vendidos
                    </a>
                </li>


                 <li>
                    <a  href="{{route('indicador.categoria.favorita')}}">
                        - Categoría preferida de los clientes
                    </a>
                </li>


                 <li>
                    <a  href="{{route('indicador.devolucion.productos')}}">
                        - Mayor Devolución de Productos
                    </a>
                </li>

                 <li>
                    <a  href="{{route('indicador.cantidad.devuelto')}}">
                        - Cantidad de productos con mayor dev
                    </a>
                </li>


            </ul>
        </li>
        <li><a href="#">Quiénes Somos</a></li>
        <li><a href="#">Equipo</a></li>
        <li><a href="#">Contacto</a></li>
    </ul>



</div>