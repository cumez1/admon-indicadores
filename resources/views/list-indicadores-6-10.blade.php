﻿<div class="classynav">
    <ul id="nav">

        <li ><a href="{{route('inicio')}}">Inicio</a></li>
        <li ><a href="{{route('dashboard.index')}}">Dashboard</a></li>
        <li><a href="#">Indicadores</a>
            <ul class="dropdown" >
                <li >
                    <a  href="{{route('indicador.vendidos.estado')}}">
                        - Tipo de productos mas vendidos por estado
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.frecuentes.estado')}}">
                        - Clientes mas frecuentes por estado 
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.menos.vendido')}}">
                        - Productos menos vendidos
                    </a>
                </li>
                <li>
                    <a  href="{{route('indicador.stock.producto')}}">
                        - Stock de productos por rango de fechas </a>
                </li>
                
                <li>
                    <a  href="{{route('indicador.ventas.empleado')}}">
                        - Rankin de empleados que mas han vendido
                    </a>
                </li>

            </ul>
        </li>
        <li><a href="#">Quiénes Somos</a></li>
        <li><a href="#">Equipo</a></li>
        <li><a href="#">Contacto</a></li>
    </ul>



</div>