<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="UMG - Sololá | Admon IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Title -->
    <title>UMG - Sololá | Admon IT Listado General</title>

    <!-- Favicon -->
    <link rel="icon" href="{{asset('uza/img/core-img/favicon.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('uza/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    
    <link href="{{asset('libs/Ionicons/css/ionicons.css')}}" rel="stylesheet">


    <style>
        .breakpoint-off .classynav ul li .dropdown {
            width: 410px;
        }

        .texto-custom{
            color: #343a40;
            font-size: 32px;
            font-family: "Lato", "Helvetica Neue", Arial, sans-serif;
            font-weight: 700;
        }
        .pd-menu{
            padding-top: 10px;
        }

    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>


    <!-- ***** Header Area Start ***** -->
    @include('nav-bar')
    <!-- ***** Header Area End ***** -->
    
    <div style="margin-top: 20px">
        <br>
    </div>

    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
              <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Listado General</li>
              </ol>
              <h6 class="slim-pagetitle">Listado General de Indicadores</h6>
            </div><!-- slim-pageheader -->

            <!-- Contenido --> 
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <a  href="{{route('dashboard.index')}}"
                        class="btn btn-primary btn-block float-right">
                        <i class="fa fa-tachometer" aria-hidden="true"></i> Regresar al Dashborad
                    </a>
                    <br>
                </div>
            </div>
            

            <ol class="dropdown" >
                <li class="pd-menu">
                    <a class="btn btn-outline-primary" href="{{route('indicador.productos.vendidos')}}">
                        1.  <i class="fa fa-bar-chart" aria-hidden="true"></i> Ranking de productos más vendidos
                    </a>
                </li>
                <li class="pd-menu">
                    <a class="btn btn-outline-secondary" href="{{route('indicador.ventas.mes')}}">
                        2.  <i class="fa fa-area-chart" aria-hidden="true"></i> Ranking de ventas por mes</a>
                </li>
                <li class="pd-menu">
                    <a class="btn btn-outline-success" href="{{route('indicador.clientes.frecuentes')}}">
                        3.  <i class="fa fa-line-chart" aria-hidden="true"></i> Ranking de clientes más frecuentes </a> 
                </li>
                <li class="pd-menu">
                    <a class="btn btn-outline-danger" href="{{route('indicador.comparacion.ventas')}}">
                        4.  <i class="fa fa-pie-chart" aria-hidden="true"></i> Comparaciones de ventas por año </a>
                </li>
                
                <li class="pd-menu">
                    <a class="btn btn-outline-warning" href="{{route('indicador.ciudad.mayor')}}">
                        5.  <i class="fa fa-bar-chart" aria-hidden="true"></i> Estado que consume más productos. </a>
                </li>

                <li class="pd-menu">
                    <a class="btn btn-outline-info" href="{{route('indicador.vendidos.estado')}}">
                        6.  <i class="fa fa-area-chart" aria-hidden="true"></i> Tipo de productos que son más vendidos por estado
                    </a>
                </li>
                <li class="pd-menu">
                    <a class="btn btn-outline-dark" href="{{route('indicador.frecuentes.estado')}}">
                        7.  <i class="fa fa-line-chart" aria-hidden="true"></i> Clientes más frecuentes por estado
                    </a>
                </li>
                <li class="pd-menu">
                    <a class="btn btn-outline-primary" href="{{route('indicador.menos.vendido')}}">
                        8.  <i class="fa fa-pie-chart" aria-hidden="true"></i> Productos menos vendidos
                    </a>
                </li>
                <li class="pd-menu">
                    <a class="btn btn-outline-secondary" href="{{route('indicador.stock.producto')}}">
                        9.  <i class="fa fa-bar-chart" aria-hidden="true"></i> Stock de productos por rango de fechas </a>
                </li>
                
                <li class="pd-menu">
                    <a class="btn btn-outline-success" href="{{route('indicador.ventas.empleado')}}">
                        10. <i class="fa fa-area-chart" aria-hidden="true"></i> Rankin de empleados que más han vendido
                    </a>
                </li>
                
                 <li class="pd-menu">
                    <a class="btn btn-outline-danger" href="{{route('indicador.mas.vendidos')}}">
                        11. <i class="fa fa-line-chart" aria-hidden="true"></i> Producto más vendido
                    </a>
                </li>

                <li class="pd-menu">
                    <a class="btn btn-outline-warning" href="{{route('indicador.montos.totales')}}">
                        12. <i class="fa fa-pie-chart" aria-hidden="true"></i> Monto total de productos vendidos por un rango de fecha
                    </a>
                </li>


                 <li class="pd-menu">
                    <a class="btn btn-outline-info" href="{{route('indicador.categoria.favorita')}}">
                        13. <i class="fa fa-bar-chart" aria-hidden="true"></i> Categoría con más clientes
                    </a>
                </li>


                 <li class="pd-menu">
                    <a class="btn btn-outline-success" href="{{route('indicador.devolucion.productos')}}">
                        14. <i class="fa fa-area-chart" aria-hidden="true"></i> Producto que más ha sido devuelto
                    </a>
                </li>

                 <li class="pd-menu">
                    <a class="btn btn-outline-primary" href="{{route('indicador.cantidad.devuelto')}}">
                        15. <i class="fa fa-line-chart" aria-hidden="true"></i> Cantidad de productos devueltos en un rango de fecha
                    </a>
                </li>
            </ol>
                        

        </div><!-- container -->
    </div><!-- slim-mainpanel -->


    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

            </div>
            <div class="row" style="margin-bottom: 30px;">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UMG - Sololá
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- ******* All JS Files ******* -->
    <!-- jQuery js -->
    <script src="{{asset('uza/js/jquery.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('uza/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('uza/js/bootstrap.min.js')}}"></script>
    <!-- All js -->
    <script src="{{asset('uza/js/uza.bundle.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('uza/js/default-assets/active.js')}}"></script>



</body>

</html>