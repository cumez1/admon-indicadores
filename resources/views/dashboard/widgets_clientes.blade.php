<div class="card-deck card-deck-sm mg-t-20 mg-x-0">
          
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-dos.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ranking clientes frecuentes</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br><br><br>

            <p>Las empresas con un solido prospecto de negocio tienen muy en cuenta la importancia de los clientes, puesto que sin estos la economía de esta empresa podría venirse abajo...</p>
            <a href="{{route('indicador.clientes.frecuentes')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->


    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-siete.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Clientes más frecuentes por ciudad</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br>
            <i class="fa fa-globe" aria-hidden="true"></i> Estado<br>
            <i class="fa fa-map-marker" aria-hidden="true"></i> Ciudad<br><br>

            <p>Si bien conocemos quien (cliente) es el que mas ha consumido en nuestra empresa, podemos obtener más información para ofrecerles promociones específicas...</p>
            <a href="{{route('indicador.frecuentes.estado')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->


</div><!-- card-deck -->
