<div class="card-deck card-deck-sm mg-t-20 mg-x-0">
          
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-dos.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ranking de ventas por mes</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br><br>

            <p>Este indicador muestra las ventas de todo un año en específico, mostrando los montos totales de las ventas de los 12 meses, mostrándolos individualmente para evitar confusiones y para una mayor especificidad.</p>
            <a href="{{route('indicador.ventas.mes')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->


    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-cuatro.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Comparaciones de ventas por año</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br> <br>

            <p>Este indicador muestra el monto total de las ventas realizada en una determinada lista de años, así como la cantidad de ventas que en estos se concretaron.</p><br>
            <a href="{{route('indicador.comparacion.ventas')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->

    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-cinco.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ciudadades con mayor consumo de productos</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>¿En qué país o estado deberíamos concentrarnos más? Para bien o para mal esta pregunta supone utilizar datos específicos que nos puedan mostrar el monto total consumido por diferentes ciudades</p>
            <a href="{{route('indicador.ciudad.mayor')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->

</div><!-- card-deck -->

<br>
<div class="card-deck card-deck-sm mg-t-20 mg-x-0">
          
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-diez.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Rankin de empleados que más han vendido</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>Toda empresa cuenta con una cultura laboral que puede ser buena o mala para el desempeño de los empleados, ¿Se debería premiar al empleado más eficiente?...</p>
            <a href="{{route('indicador.ciudad.mayor')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->




    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-trece.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Categoría favorita de los clientes</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br> 
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p> Este indicador muestra un Rankin de las categorías más populares en un determinado mes y año, así como el monto generado por esta...</p><br>
            <a href="{{route('indicador.categoria.favorita')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->


    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-quince.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Cantidad de productos con mayor devolución [rango de fechas]</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br> 
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p> Este indicador muestra información en un rango de fechas de cuanta cantidad de producto 
                fue devuelta...</p><br>
            <a href="{{route('indicador.categoria.favorita')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->

</div><!-- card-deck -->

<br>
<div class="card-deck card-deck-sm mg-t-20 mg-x-0">
          
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-catorce.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Producto que más ha sido devuelto</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>Como en toda empresa dedicada a las ventas, existirán inconvenientes con los productos ofrecidos a los clientes, obtener información acerca de los productos que mas...</p>
            <a href="{{route('indicador.devolucion.productos')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->




    <div class="card tx-center" style="border-width: 0px">
        
    </div><!-- card -->


    <div class="card tx-center" style="border-width: 0px">
        
    </div><!-- card -->

</div><!-- card-deck -->
