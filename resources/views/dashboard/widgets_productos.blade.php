<div class="card-deck card-deck-sm mg-t-20 mg-x-0">
          
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-uno.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ranking productos mas vendido</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>Toda empresa dedicada a las ventas tendrá como prioridad obtener información que le sea relevante para analizar la aceptación que han tenido los productos que estos ofrecen...</p>
            <a href="{{route('indicador.productos.vendidos')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->



    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-seis.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Tipo de productos mas vendidos por ciudad</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>Muy bien, conocemos ya que estado consume mas en nuestra empresa, pero ¿Qué es lo que consume?, que tipo de productos deberíamos enviar mas a...</p>
            <a href="{{route('indicador.vendidos.estado')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->

    
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-ocho.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ranking productos menos vendido</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>Con el indicador numero 1 obtenemos información de aquellos productos que mas han generado para la empresa y poder manipular los mismos para obtener aun...</p>
            <a href="{{route('indicador.vendidos.estado')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->


</div><!-- card-deck -->


<div class="card-deck card-deck-sm mg-t-20 mg-x-0">
          
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-nueve.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ranking tock de productos</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>Información de este tipo puede ser de suma importancia para cuadrar cuentas, para determinar cuanto de producto se debe adquirir para mese próximos...</p>
            <a href="{{route('indicador.stock.producto')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->


    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-once.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Ranking de productos con más venta</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br>
            <i class="fa fa-list-ol" aria-hidden="true"></i> Limite de registros<br><br>

            <p>El indicador 1 nos muestra información acerca del producto más vendido, pero ¿Y si necesitamos segmentar este Rankin? Debemos reducir la cantidad de registros...</p>
            <a href="{{route('indicador.mas.vendidos')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->

    
    <div class="card tx-center">
        <div class="card-body pd-40">
            <div class="d-flex justify-content-center mg-b-30">
                <img src="{{asset('img/indicador-doce.png')}}" width="90%" height="90%">
            </div>
            <h6 class="tx-md-20 tx-inverse mg-b-20">Monto total de productos vendidos por un rango de fechas</h6>
            <h6>PARAMETROS</h6>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Inicial<br>
            <i class="fa fa-calendar-o" aria-hidden="true"></i> Fecha Final <br> <br>

            <p>Este indicador nos entrega el monto total de ventas en un intervalo de fecha, es decir, nos puede mostrar cuanto se vendido en un lapso de 10 años, 5, 4...</p>
            <a href="{{route('indicador.montos.totales')}}" class="btn btn-primary btn-block">Ir al indicador</a>
        </div><!-- card -->
    </div><!-- card -->



</div><!-- card-deck -->
