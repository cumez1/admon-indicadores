<div class="dash-headline">
  <div class="dash-headline-left">
    <div class="dash-headline-item-one">
      <div id="chartArea1" class="dash-chartist"></div>
      <div class="dash-item-overlay">
        <h4 class="texto-custom"><span class="tx-20">VENTAS</span></h4>
        <p class="earning-label">Indicadores de ventas</p>
        <p class="earning-desc">Los indicadores de ventas por rango de fechas, ranking de ventas, comparaciones de ventas por año...</p>
        <a href="{{route('dashboard.ventas')}}" class="statement-link">Ver indicadores <i class="fa fa-angle-right mg-l-5"></i></a>
      </div>
    </div><!-- dash-headline-item-one -->
  </div><!-- dash-headline-left -->

  <div class="dash-headline-right">
    <div class="dash-headline-right-top">
      <div class="dash-headline-item-two">
        <div id="chartMultiBar1" class="chart-rickshaw"></div>
        <div class="dash-item-overlay">
          <h4><span class="tx-20">PRODUCTOS</span></h4>
          <p class="item-label">Indicadores de Productos</p>
          <p class="item-desc">Conocer los productos más vendidos, devueltos nos facilita la toma de decisiones </p>
          <a href="{{route('dashboard.productos')}}" class="report-link">Ver indicadores <i class="fa fa-angle-right mg-l-5"></i></a>
        </div>
      </div><!-- dash-headline-item-two -->
    </div><!-- dash-headline-right-top -->
    
     <div class="dash-headline-right-top">
      <div class="dash-headline-item-two">
        
        <span id="sparkline3" class="sparkline wd-100p" style="width: 600px;">16,10,4,8,9,10,3,15,3,5,7,3,2,2,3,4</span>
        <div class="dash-item-overlay">
          <h4><span class="tx-20">CLIENTES</span></h4>
          <p class="item-label">Indicadores de Clientes</p>
          <p class="item-desc">Conocer los clientes más frecuentes, clientes por estado, compras por caterogia... </p>
          <a href="{{route('dashboard.clientes')}}" class="report-link">Ver indicadores <i class="fa fa-angle-right mg-l-5"></i></a>
        </div>
      </div><!-- dash-headline-item-two -->
    </div><!-- dash-headline-right-top -->

  </div><!-- wd-50p -->
</div><!-- d-flex ht-100v -->