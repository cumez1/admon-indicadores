<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="UMG - Sololá | Admon IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Title -->
    <title>UMG - Sololá | Quines Somos</title>

    <!-- Favicon -->
    <link rel="icon" href="{{asset('uza/img/core-img/favicon.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('uza/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    
        <link href="{{asset('libs/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('libs/chartist/css/chartist.css')}}" rel="stylesheet">
    <link href="{{asset('libs/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet">

    <style>
        .breakpoint-off .classynav ul li .dropdown {
            width: 410px;
        }

        .texto-custom{
            color: #343a40;
            font-size: 32px;
            font-family: "Lato", "Helvetica Neue", Arial, sans-serif;
            font-weight: 700;
        }

    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>


    <!-- ***** Header Area Start ***** -->
    @include('nav-bar')
    <!-- ***** Header Area End ***** -->
    
    <div style="margin-top: 20px">
        <br>
    </div>

    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
              <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('inicio')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Equipo</li>
              </ol>
              <h6 class="slim-pagetitle">Equipo</h6>
            </div><!-- slim-pageheader -->
    
            <!-- Contenido -->  
            <div class="row">
                <div class="col-lg-12">
                
                    <h1>EQUIPO</h1>
                    <p>Somos un equipo conformado por especialistas en ventas y procesos comerciales, así como desarrolladores de alto rendimiento para desarrollar software especializado para uso gerencial con más de 20 años de experiencia en el desarrollo y mejora del área
                    </p>
                </div>
            </div> 

            <div class="row">
                <div class="col-lg-12">
                    
                    <h2>HEBER JONATHAN REANDA SOJUEL</h2>
                    <h5>Carné No 2290-15-9801</h5>
                    <img width="20%" src="{{ asset('img/heber.jpg') }}" class="img-thumbnail rounded float-left" alt="HEBER JONATHAN REANDA SOJUEL">
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <h2>MIGUELANGEL JACINTO GUARCAS</h2>
                    <h5>Carné No 2290-15-11547</h5>
                    <img width="20%" src="{{ asset('img/mikke.jpg') }}" class="img-thumbnail rounded float-left" alt="MIGUELANGEL JACINTO GUARCAS">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    
                    <h2>NICOLAS CUMEZ NIMACACHI</h2>
                    <h5>Carné No 2290-12-6364</h5>
                    <img width="20%" src="{{ asset('img/nicolas.jpg') }}" class="img-thumbnail rounded float-left" alt="NICOLAS CUMEZ NIMACACHI">
                </div>
            </div>
           

        </div><!-- container -->
    </div><!-- slim-mainpanel -->


    
    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

            </div>
            <div class="row" style="margin-bottom: 30px;">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UMG - Sololá
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- ******* All JS Files ******* -->
    <!-- jQuery js -->
    <script src="{{asset('uza/js/jquery.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('uza/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('uza/js/bootstrap.min.js')}}"></script>
    <!-- All js -->
    <script src="{{asset('uza/js/uza.bundle.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('uza/js/default-assets/active.js')}}"></script>

    <script src="{{asset('libs/chartist/js/chartist.js')}}"></script>
    <script src="{{asset('libs/d3/js/d3.js')}}"></script>
    <script src="{{asset('libs/rickshaw/js/rickshaw.min.js')}}"></script>
    <script src="{{asset('libs/jquery.sparkline.bower/js/jquery.sparkline.min.js')}}"></script>


    <script src="{{asset('js/dashboard.js')}}"></script>

</body>

</html>