<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="UMG - Sololá | Admon IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Title -->
    <title>UMG - Sololá | Admon IT</title>

    <link rel="icon" href="{{asset('uza/img/core-img/favicon.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('uza/style.css')}}">
    
    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <style>
        .breakpoint-off .classynav ul li .dropdown {
            width: 410px;
        }
    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>


    <!-- ***** Header Area Start ***** -->
    @include('nav-bar')
    <!-- ***** Header Area End ***** -->
    <section class="uza-services-area" style="padding-top: 100px;">
        <div class="container">
            
            @section('nombre_indicador', 'Producto que más ha sido devuelto')
            @section('dashboard_nombre', 'Dashboard Ventas')
            @section('dashboard_ruta', route('dashboard.ventas'))
            @include('dashboard.navbar-dashboard')
            

            <div class="row">
                <div class="col-md-12">
                    <form>
                        <div class="row">                        
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <label for="top">Seleccione el ranking de productos</label>
                                    <select class="form-control" id="top_registro" aria-describedby="top_descrip">
                                        <option value="5">5</option>
                                        <option value="10" selected>10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <small id="top_descrip" class="form-text text-muted">Es obligatorio seleccionar un limite</small>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group" style="padding-top: 28px;">
                                    <button id="opGenerar" type="button" class="btn btn-primary btn-block"><i class="fa fa-search" aria-hidden="true"></i> Generar consulta</button>
                                </div>
                            </div>
                        </div>

                        <br>
                    </form>
                       
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <table class="table" id="listadoTabla">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Código producto</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Cantidad devuelto</th>
                                <th scope="col">Total costo devuelto (Dolares $)</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12 col-md-12"> 
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Producto que más ha sido devuelto</h5>
                            <h6 class="card-subtitle mb-2 text-muted"></h6>
                            <p class="card-text">Como en toda empresa dedicada a las ventas, existirán inconvenientes con los productos ofrecidos a los clientes, obtener información acerca de los productos que mas han sido devueltos puede ser de utilidad para determinar si debemos cambiar de proveedor o dejar de vender ese tipo de productos.<br><br>
                            Este indicador muestra un Rankin de productos que más han sido devueltos en un intervalo de fechas específico.


                            </p>
                            <br>
                            <div id="chart_div" ></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

            </div>
            <div class="row" style="margin-bottom: 30px;">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UMG - Sololá
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- ******* All JS Files ******* -->

    <script src="{{asset('uza/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('uza/js/bootstrap.min.js')}}"></script>
    <!-- All js -->
    <script src="{{asset('uza/js/uza.bundle.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('uza/js/default-assets/active.js')}}"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('libs/datatables/datatables.min.css')}}"/>
 
    <script type="text/javascript" src="{{asset('libs/datatables/datatables.min.js')}}"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.es-es.js" type="text/javascript"></script>
    
    
    <link media="all" type="text/css" rel="stylesheet" href="{{asset('toastr/toastr.min.css')}}">
    <script src="{{asset('toastr/toastr.min.js')}}"></script>
    <script src="{{asset('js/app_umg.js')}}"></script>
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};


        function callAjax(url,datos, response){
            $.ajax({
                type: "POST",
                url: url,
                dataType : 'json',
                headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
                data: datos,
                success: response,
                error:response           
            });                
        }
    </script>
    
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {
            var colorBar = 'stroke-color: #f2af4d; stroke-width: 4; fill-color: #ffd670';
            var data = google.visualization.arrayToDataTable([
                ['Producto', 'Cantidad devuelto',{ role: 'style' }],
                ['',  0, colorBar]
            ]);


            var options = {
                title: 'Producto que más ha sido devuelto',
                chartArea: {width: '65%'},
                vAxis: {
                    title: 'Cantidad de producto devuelto',
                    titleTextStyle: {
                        color: 'black',
                        bold: true,
                        italic: false
                    }
                },
                width: '100%',
                height: 600,
                legend: { position: "none" }
                
            };




            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        var datepicker, config;
        config = {
            locale: 'es-es',
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy'
        };

        $(document).ready(function () {
            $('#fh_inicial').datepicker(config);
            $('#fh_final').datepicker(config);
            $('#listadoTabla').DataTable({
                language: {
                    url: "{{asset('js/lan-es-datatable.json')}}"
                }
            });

            $('#opGenerar').on('click', function() {
                generarDatatable();
            });
        });

        function number_format(number,decimals,dec_point,thousands_sep) {
            number = number.replace(/,/g, "");
            number  = number*1;
            var str = number.toFixed(decimals?decimals:0).toString().split('.');
            var parts = [];
            for ( var i=str[0].length; i>0; i-=3 ) {
                parts.unshift(str[0].substring(Math.max(0,i-3),i));
            }
            str[0] = parts.join(thousands_sep?thousands_sep:',');
            return str.join(dec_point?dec_point:'.');
        }


        function generarDatatable(){

            table = $('#listadoTabla').DataTable();
            table.destroy();

            table = $('#listadoTabla').DataTable({
                language: {
                    url: "{{asset('js/lan-es-datatable.json')}}"
                },
                bFilter : false, //oculta filtros            
                ordering: false,
                processing: true,
                serverSide: true,
                pageLength: 12,
                lengthMenu: {{ config('constantes.datatableListRows') }},
                ajax: {
                    url: "{{ route('indicador.devolucion.productos.listado') }}",
                    type: 'POST',
                    data : function (d) {                    
                        d._token = "{{ csrf_token() }}";  
                        d.top_registro = $("#top_registro").val(); 
                    },
                    error: function (xhr, error, thrown) {

                        if (xhr.status == 422) {
                            toastrWarningList(xhr.responseJSON.errors);
                            $("#listadoTabla_processing").hide();
                            $("#listadoTabla_wrapper > .clear").hide();
                        }         
                    }
                },

                columns: [
                    {data: 'DT_RowIndex',  className: 'text-center', width: '5%'},
                    {data: 'codigo_item', className: 'text-left', width: '15%'},
                    {data: 'nombre_producto', className: 'text-left', width: '40%'},
                    {data: 'cantidad_producto_devuelto', className: 'text-left', width: '20%',render: $.fn.dataTable.render.number(',', '.', 0)},
                    {data: 'total_devuelto', className: 'text-center', width: '20%',render: $.fn.dataTable.render.number(',', '.', 2)}
                ],
                initComplete: function(settings, json) {
                    $('#listadoTabla').find('.sorting_asc').each(function(){
                        $(this).removeClass("sorting_asc");
                    });
                    //getDataGrafica(json);
                },
                fnDrawCallback: function( settings ) {
                    getDataGrafica(settings.json);
                }
            });

            $.fn.dataTable.ext.errMode = 'throw';

            $('#top_registro').change( function() { 
                table.page.len( $(this).val() ).draw();
            });
        };

        function getDataGrafica(json){

            var data = json.data;
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(pintarGrafica(data));

        }

        function pintarGrafica(data){

            var datagenerada = [];
            var colorBar = 'stroke-color: #f2af4d; stroke-width: 4; fill-color: #ffd670';

            datagenerada.push(['Producto', 'Cantidad devuelto',{ role: 'style' }]);

            $.each(data, function( index, value ) {
                datagenerada.push(
                    [value.nombre_producto, 
                    parseInt(value.cantidad_producto_devuelto),
                    colorBar
                    ]);
            });

            console.log(datagenerada);
            
            var data = google.visualization.arrayToDataTable(datagenerada);

            var options = {
                title: 'Producto que más ha sido devuelto',
                chartArea: {width: '65%'},
                vAxis: {
                    title: 'Cantidad de producto devuelto',
                    titleTextStyle: {
                        color: 'black',
                        bold: true,
                        italic: false
                    }
                },
                width: '100%',
                height: 600,
                legend: { position: "none" }
                
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);

        }

    </script>

</body>

</html>