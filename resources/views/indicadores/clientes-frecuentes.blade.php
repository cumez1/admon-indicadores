<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="UMG - Sololá | Admon IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Title -->
    <title>UMG - Sololá | Admon IT</title>

    <link rel="icon" href="{{asset('uza/img/core-img/favicon.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('uza/style.css')}}">
    
    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <style>
        .breakpoint-off .classynav ul li .dropdown {
            width: 410px;
        }
    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>


    <!-- ***** Header Area Start ***** -->
    @include('nav-bar')
    <!-- ***** Header Area End ***** -->

    
    <section class="uza-services-area" style="padding-top: 100px;">
        
        <div class="container">
            @section('nombre_indicador', 'Ranking de clientes frecuentes')
            @section('dashboard_nombre', 'Dashboard Clientes')
            @section('dashboard_ruta', route('dashboard.clientes'))
            @include('dashboard.navbar-dashboard')
            
            <div class="row">
                <div class="col-md-12">
                    <form>
                        <div class="row">                        
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <label for="fh_inicial">Fecha inicial</label>
                                    <input value="01/01/2001" type="text" class="form-control" id="fh_inicial" aria-describedby="fh_i_descrip">
                                    <small id="fh_i_descrip" class="form-text text-muted">Es obligatorio ingresar la fecha inicial</small>
                                </div>
                                <div class="gj-clear-both"></div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <label for="fh_final">Fecha final</label>
                                    <input value="31/12/2001" type="text" class="form-control" id="fh_final" aria-describedby="fh_f_descrip">
                                    <small id="fh_f_descrip" class="form-text text-muted">Es obligatorio ingresar la fecha final</small>
                                </div>
                                <div class="gj-clear-both"></div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <label for="top">Seleccione el ranking de clientes</label>
                                    <select class="form-control" id="top_registro" aria-describedby="top_descrip">
                                        <option value="5">5</option>
                                        <option value="10" selected>10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <small id="top_descrip" class="form-text text-muted">Es obligatorio seleccionar un limite</small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <button id="opGenerar" type="button" class="btn btn-primary btn-block"><i class="fa fa-search" aria-hidden="true"></i> Generar consulta</button>
                            </div>
                        </div>

                        <br>
                    </form>
                       
                </div>
            </div>

            <div class="row">
                <div class="col-lg-9 col-md-9">
                    <table class="table" id="listadoTabla">
                        <thead>
                            <tr>
                                <th scope="col" class="txt-center">#</th>
                                <th scope="col" class="txt-center">Codigo Cliente</th>
                                <th scope="col" class="txt-center">Nombre Completo</th>
                                <th scope="col" class="txt-center">Número de Compras</th>
                                <th scope="col" class="txt-center">Total en Compras ($)</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12 col-md-12"> 
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Ranking de clientes frecuentes</h5>
                            <h6 class="card-subtitle mb-2 text-muted"></h6>
                            <p class="card-text">Las empresas con un solido prospecto de negocio tienen muy en cuenta la importancia de los clientes, puesto que sin estos la economía de esta empresa podría venirse abajo y quedar echa pedazos, por eso saber que clientes son los que mas frecuentan a la empresa puede vital para hacerles un seguimiento he incluso verificar su nivel de satisfacción, pues no hay mejor publicidad que aquella que se da de boca en boca.<br><br>
                            
                            Este indicador muestra los datos básicos del cliente, así como el monto total que el cliente ha gastado y la cantidad de veces que ha comprado.

                            </p>
                            <br>
                            <div id="chart_div" ></div>

                        </div>
                    </div>
                </div>
            </div>

            

            

        </div>
    </section>
    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

            </div>
            <div class="row" style="margin-bottom: 30px;">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UMG - Sololá
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- ******* All JS Files ******* -->

    <script src="{{asset('uza/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('uza/js/bootstrap.min.js')}}"></script>
    <!-- All js -->
    <script src="{{asset('uza/js/uza.bundle.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('uza/js/default-assets/active.js')}}"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('libs/datatables/datatables.min.css')}}"/>
 
    <script type="text/javascript" src="{{asset('libs/datatables/datatables.min.js')}}"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.es-es.js" type="text/javascript"></script>
    
    
    <link media="all" type="text/css" rel="stylesheet" href="{{asset('toastr/toastr.min.css')}}">
    <script src="{{asset('toastr/toastr.min.js')}}"></script>
    <script src="{{asset('js/app_umg.js')}}"></script>
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};


        function callAjax(url,datos, response){
            $.ajax({
                type: "POST",
                url: url,
                dataType : 'json',
                headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
                data: datos,
                success: response,
                error:response           
            });                
        }
    </script>
    
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {
            var colorBar = 'stroke-color: #356493; stroke-width: 4; fill-color: #a5bacf';
            var data = google.visualization.arrayToDataTable([
                ['Cliente', 'Número de Compras',{ role: 'style' },{ role: 'annotation' } ],
                [   '', 0,colorBar,'']
            ]);

            var options = {
                title: 'Ranking de clientes frecuentes',
                chartArea: {width: '65%'},
                hAxis: {
                    title: 'Número de compras realizadas',
                    format: '0'
                },
                vAxis: {
                    title: 'Clientes',
                    titleTextStyle: {
                        color: 'black',
                        bold: true,
                        italic: false
                    },
                    format: '0'
                },
                width: '100%',
                height: 600,
                legend: { position: "none" },
                
            };
                

          var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

          chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        var datepicker, config;
        config = {
            locale: 'es-es',
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy'
        };

        $(document).ready(function () {
            $('#fh_inicial').datepicker(config);
            $('#fh_final').datepicker(config);
            
            $('#listadoTabla').DataTable({
                language: {
                    url: "{{asset('js/lan-es-datatable.json')}}"
                }
            });

            $('#opGenerar').on('click', function() {
                generarDatatable();
            });
        });

        function generarDatatable(){

            table = $('#listadoTabla').DataTable();
            table.destroy();

            table = $('#listadoTabla').DataTable({
                language: {
                    url: "{{asset('js/lan-es-datatable.json')}}"
                },
                bFilter : false, //oculta filtros            
                ordering: false,
                processing: true,
                serverSide: true,
                pageLength: 10,
                lengthMenu: {{ config('constantes.datatableListRows') }},
                ajax: {
                    url: "{{ route('indicador.clientes.frecuentes.listado') }}",
                    type: 'POST',
                    data : function (d) {                    
                        d._token = "{{ csrf_token() }}";                    
                        d.fh_inicial = $("#fh_inicial").val();
                        d.fh_final = $("#fh_final").val();
                        d.top_registro = $("#top_registro").val();
                    },
                    error: function (xhr, error, thrown) {

                        if (xhr.status == 422) {
                            toastrWarningList(xhr.responseJSON.errors);
                            $("#listadoTabla_processing").hide();
                            $("#listadoTabla_wrapper > .clear").hide();
                        }         
                    }
                },
                columns: [
                    {data: 'DT_RowIndex',  className: 'text-center', width: '5%'},
                    {data: 'codigo_cliente', className: 'text-left', width: '15%'},
                    {data: 'nombre_completo', className: 'text-left', width: '40%'},
                    {data: 'numero_compras', className: 'text-center', width: '10%'},
                    {data: 'total_compra', className: 'text-right', width: '30%'}
                ],
                initComplete: function(settings, json) {
                    $('#listadoTabla').find('.sorting_asc').each(function(){
                        $(this).removeClass("sorting_asc");
                    });
                    //getDataGrafica(json);
                },
                fnDrawCallback: function( settings ) {
                    getDataGrafica(settings.json);
                }
            });

            $.fn.dataTable.ext.errMode = 'throw';

            $('#top_registro').change( function() { 
                table.page.len( $(this).val() ).draw();
            });
        };

        function getDataGrafica(json){

            var data = json.data;
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(pintarGrafica(data));

        }

        function pintarGrafica(data){

            var datagenerada = [];
            var colorBar = 'stroke-color: #356493; stroke-width: 4; fill-color: #a5bacf';

            datagenerada.push(['Cliente', 'Número de Compras',{ role: 'style' },{ role: 'annotation' } ],);

            $.each(data, function( index, value ) {
                datagenerada.push(
                    [value.nombre_completo, 
                    parseInt(value.numero_compras),
                    'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF',
                    '$ '+value.total_compra
                    ]);
            });

            console.log(datagenerada);
            var data = google.visualization.arrayToDataTable(datagenerada);

            var options = {
                title: 'Ranking de clientes frecuentes',
                chartArea: {width: '65%'},
                hAxis: {
                    title: 'Número de compras realizadas',
                    format: '0'
                },
                vAxis: {
                    title: 'Clientes',
                    titleTextStyle: {
                        color: 'black',
                        bold: true,
                        italic: false
                    },
                    format: '0'
                },
                width: '100%',
                height: 500,
                legend: { position: "none" },
                
            };

          var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

          chart.draw(data, options);

        }

    </script>

</body>

</html>