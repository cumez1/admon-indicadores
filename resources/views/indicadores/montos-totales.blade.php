<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="UMG - Sololá | Admon IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Title -->
    <title>UMG - Sololá | Admon IT</title>

    <link rel="icon" href="{{asset('uza/img/core-img/favicon.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('uza/style.css')}}">
    
    
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <style>
        .breakpoint-off .classynav ul li .dropdown {
            width: 410px;
        }
    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>


    <!-- ***** Header Area Start ***** -->
    @include('nav-bar')
    <!-- ***** Header Area End ***** -->


    <section class="uza-services-area" style="padding-top: 100px;">
        <div class="container">
            @section('nombre_indicador', 'Monto total de productos vendidos por un rango de fechas')
            @section('dashboard_nombre', 'Dashboard Productos')
            @section('dashboard_ruta', route('dashboard.productos'))
            @include('dashboard.navbar-dashboard')
            

            <div class="row">
                <div class="col-md-12">
                    <form>
                        <div class="row">                        
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <label for="fh_inicial">Fecha inicial</label>
                                    <input value="01/01/2001" type="text" class="form-control" id="fh_inicial" aria-describedby="fh_i_descrip">
                                    <small id="fh_i_descrip" class="form-text text-muted">Es obligatorio ingresar la fecha inicial</small>
                                </div>
                                <div class="gj-clear-both"></div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <label for="fh_final">Fecha final</label>
                                    <input value="31/12/2001" type="text" class="form-control" id="fh_final" aria-describedby="fh_f_descrip">
                                    <small id="fh_f_descrip" class="form-text text-muted">Es obligatorio ingresar la fecha final</small>
                                </div>
                                <div class="gj-clear-both"></div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group" style="padding-top: 28px;">
                                    <button id="opGenerar" type="button" class="btn btn-primary btn-block">Generar consulta</button>
                                </div>
                            </div>
                        </div>

                        <br>
                    </form>
                       
                </div>
            </div>

            <div class="row">
                <div class="col-lg-9 col-md-9">
                    <table class="table" id="listadoTabla">
                        <thead>
                            <tr>
                                <th scope="col" class="txt-center">#</th>
                                <th scope="col" class="txt-center">Código</th>
                                <th scope="col" class="txt-center">Nombre del producto</th>
                                <th scope="col" class="txt-center">Categoria del producto</th>
                                <th scope="col" class="txt-center">Monto total de ventas ($)</th>
                                <th scope="col" class="txt-center">Total de producto vendido</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12 col-md-12"> 
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Monto total de productos vendidos por un rango de fechas</h5>
                            <h6 class="card-subtitle mb-2 text-muted"></h6>
                            <p class="card-text">Si bien el indicador 2 y el indicador 4 nos dan información acerca de los montos vendidos.<br><br>
                            
                            Este indicador nos entrega el monto total de ventas en un intervalo de fecha, es decir, nos puede mostrar cuanto se vendido en un lapso de 10 años, 5, 4 o los que se necesiten para obtener los resultados deseados.

                            </p>
                            <br>
                            <div id="chart_div" ></div>

                        </div>
                    </div>
                </div>
            </div>

            

            

        </div>
    </section>
    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

            </div>
            <div class="row" style="margin-bottom: 30px;">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UMG - Sololá
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- ******* All JS Files ******* -->

    <script src="{{asset('uza/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('uza/js/bootstrap.min.js')}}"></script>
    <!-- All js -->
    <script src="{{asset('uza/js/uza.bundle.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('uza/js/default-assets/active.js')}}"></script>

    <link rel="stylesheet" type="text/css" href="{{asset('libs/datatables/datatables.min.css')}}"/>
 
    <script type="text/javascript" src="{{asset('libs/datatables/datatables.min.js')}}"></script>

    <script src="https://unpkg.com/gijgo@1.9.13/js/messages/messages.es-es.js" type="text/javascript"></script>
    
    
    <link media="all" type="text/css" rel="stylesheet" href="{{asset('toastr/toastr.min.css')}}">
    <script src="{{asset('toastr/toastr.min.js')}}"></script>
    <script src="{{asset('js/app_umg.js')}}"></script>
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};


        function callAjax(url,datos, response){
            $.ajax({
                type: "POST",
                url: url,
                dataType : 'json',
                headers: {'X-CSRF-TOKEN': "{{csrf_token()}}"},
                data: datos,
                success: response,
                error:response           
            });                
        }
    </script>
    
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBasic);

        function drawBasic() {
            var colorBar = 'stroke-color: #356493; stroke-width: 4; fill-color: #a5bacf';
            var data = google.visualization.arrayToDataTable([
                ['Producto', 'Monto total vendido',{ role: 'style' },{ role: 'annotation' } ],
                [   '', 0,colorBar,'']
            ]);

            var options = {
                title: 'Monto total de productos vendidos por un rango de fechas',
                chartArea: {width: '65%'},
                hAxis: {
                    title: 'Número de ventas',
                    format: '0'
                },
                vAxis: {
                    title: 'Nombre del producto',
                    titleTextStyle: {
                        color: 'black',
                        bold: true,
                        italic: false
                    },
                    format: '0'
                },
                width: '100%',
                height: 600,
                legend: { position: "none" },
                
            };
                

          var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

          chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        var datepicker, config;
        config = {
            locale: 'es-es',
            uiLibrary: 'bootstrap4',
            format: 'dd/mm/yyyy'
        };

        $(document).ready(function () {
            $('#fh_inicial').datepicker(config);
            $('#fh_final').datepicker(config);
            
            $('#listadoTabla').DataTable({
                language: {
                    url: "{{asset('js/lan-es-datatable.json')}}"
                }
            });

            $('#opGenerar').on('click', function() {
                generarDatatable();
            });
        });

        function generarDatatable(){

            table = $('#listadoTabla').DataTable();
            table.destroy();

            table = $('#listadoTabla').DataTable({
                language: {
                    url: "{{asset('js/lan-es-datatable.json')}}"
                },
                bFilter : false, //oculta filtros            
                ordering: false,
                processing: true,
                serverSide: true,
                pageLength: 10,
                lengthMenu: {{ config('constantes.datatableListRows') }},
                ajax: {
                    url: "{{ route('indicador.montos.totales.listado') }}",
                    type: 'POST',
                    data : function (d) {                    
                        d._token = "{{ csrf_token() }}";                    
                        d.fh_inicial = $("#fh_inicial").val();
                        d.fh_final = $("#fh_final").val();
                    },
                    error: function (xhr, error, thrown) {

                        if (xhr.status == 422) {
                            toastrWarningList(xhr.responseJSON.errors);
                            $("#listadoTabla_processing").hide();
                            $("#listadoTabla_wrapper > .clear").hide();
                        }         
                    }
                },
                columns: [
                    {data: 'DT_RowIndex',  className: 'text-center', width: '5%'},
                    {data: 'codigo_item', className: 'text-left', width: '15%'},
                    {data: 'nombre_producto', className: 'text-left', width: '50%'},
                    {data: 'categoria', className: 'text-center', width: '50%'},
                    {data: 'total_vendido', className: 'text-center', width: '60%'},
                    {data: 'cantidad_productos_vendido', className: 'text-right', width: '60%'}
                ],
                initComplete: function(settings, json) {
                    $('#listadoTabla').find('.sorting_asc').each(function(){
                        $(this).removeClass("sorting_asc");
                    });
                    //getDataGrafica(json);
                },
                fnDrawCallback: function( settings ) {
                    getDataGrafica(settings.json);
                }
            });

            $.fn.dataTable.ext.errMode = 'throw';

            $('#top_registro').change( function() { 
                table.page.len( $(this).val() ).draw();
            });
        };

        function getDataGrafica(json){

            var data = json.data;
            google.charts.load('current', {packages: ['corechart', 'bar']});
            google.charts.setOnLoadCallback(pintarGrafica(data));

        }

        function pintarGrafica(data){

            var datagenerada = [];
            var colorBar = 'stroke-color: #356493; stroke-width: 4; fill-color: #a5bacf';

            datagenerada.push(['Producto', 'Número de ventas',{ role: 'style' },{ role: 'annotation' } ],);

            $.each(data, function( index, value ) {
                datagenerada.push(
                    [value.nombre_producto, 
                    parseInt(value.total_vendido),
                    'stroke-color: #ffc107; stroke-width: 4; fill-color: #BCC111',
                    '$ '+value.total_vendido
                    ]);
            });

            console.log(datagenerada);
            var data = google.visualization.arrayToDataTable(datagenerada);

            var options = {
                title: 'Monto total de productos vendidos por un rango de fechas ['+$("#fh_inicial").val()+' - '+$("#fh_final").val()+']',
                chartArea: {width: '65%'},
                hAxis: {
                    title: 'Número de ventas',
                    format: '0'
                },
                vAxis: {
                    title: 'Nombre del producto',
                    titleTextStyle: {
                        color: 'black',
                        bold: true,
                        italic: false
                    },
                    format: '0'
                },
                width: '100%',
                height: 500,
                legend: { position: "none" },
                
            };

          var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

          chart.draw(data, options);

        }

    </script>

</body>

</html>