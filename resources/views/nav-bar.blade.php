<header class="header-area">
    <!-- Main Header Start -->
    <div class="main-header-area">
        <div class="classy-nav-container breakpoint-off">
            <!-- Classy Menu -->
            <nav class="classy-navbar justify-content-between" id="uzaNav">

                <!-- Logo -->
                <a class="nav-brand" href="{{route('inicio')}}"><img src="{{asset('uza/img/core-img/logo.png')}}" alt=""></a>

                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">
                    <!-- Menu Close Button -->
                    <div class="classycloseIcon">
                        <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div class="classynav">
                        <ul id="nav">
                            <li class="{{ request()->is('/') ? 'current-item' : '' }}"><a href="{{route('inicio')}}">Inicio</a></li>
                            <li class="{{ request()->is('dashboard*') ||  request()->is('indicadores*') ? 'current-item' : '' }}"><a href="{{route('dashboard.index')}}">Dashboard</a></li>
                            <li class="{{ request()->is('quienes-somos') ? 'current-item' : '' }}"><a href="{{route('quienes.somos')}}">Quiénes Somos</a></li>
                            <li class="{{ request()->is('equipo') ? 'current-item' : '' }}"><a href="{{route('equipo')}}">Equipo</a></li>
                            <li class="{{ request()->is('contacto') ? 'current-item' : '' }}"><a href="{{route('contacto')}}">Contacto</a></li>
                        </ul>
                    </div>
                    <!-- Nav End -->

                </div>
            </nav>
        </div>
    </div>
</header>