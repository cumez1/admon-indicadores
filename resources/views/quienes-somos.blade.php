<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="UMG - Sololá | Admon IT">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Title -->
    <title>UMG - Sololá | Quines Somos</title>

    <!-- Favicon -->
    <link rel="icon" href="{{asset('uza/img/core-img/favicon.ico')}}">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('uza/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    
        <link href="{{asset('libs/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('libs/chartist/css/chartist.css')}}" rel="stylesheet">
    <link href="{{asset('libs/rickshaw/css/rickshaw.min.css')}}" rel="stylesheet">

    <style>
        .breakpoint-off .classynav ul li .dropdown {
            width: 410px;
        }

        .texto-custom{
            color: #343a40;
            font-size: 32px;
            font-family: "Lato", "Helvetica Neue", Arial, sans-serif;
            font-weight: 700;
        }

    </style>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="wrapper">
            <div class="cssload-loader"></div>
        </div>
    </div>


    <!-- ***** Header Area Start ***** -->
    @include('nav-bar')
    <!-- ***** Header Area End ***** -->
    
    <div style="margin-top: 20px">
        <br>
    </div>

    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
              <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('inicio')}}">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page">Quienes somos</li>
              </ol>
              <h6 class="slim-pagetitle">Quienes Somos</h6>
            </div><!-- slim-pageheader -->
    
            <!-- Contenido -->  
            <div class="row">
                <div class="col-lg-12">
                
                    <h1>¿Quiénes Somos?</h1>
                    <p>Somos una empresa que se dedica al mercadeo y comercialización de diferentes líneas de productos y servicios, nacionales e importados, dirigidos a segmentos específicos (target) a los cuales hemos decidido captar, satisfacer y servir a través de la implementación de una estrategia de marketing de especialización selectiva con una orientación a la generación de valor, al mercado y al cliente.
                    </p>
                    
                    <h2>Misión</h2>
                    <p>
                    Poner en valor la función del vendedor en la organizacion de tal forma que dicha competencia y sus responsables sean considerados área estratégica y fundamental de toda la empresa y que nuestros clientes se sientan orgullosos de serlo.
                    </p>
                    <h2>Visión</h2>
                    <p>
                    Ser reconocidos como los mayores expertos en incremento de ventas y conocidos por estar a la Vanguardia, dignificar y poner en valor la función Comercial.
                    </p>
                </div>
            </div> 

            <div class="row">
                <div class="col-lg-12">
                    
                    <h2>Nuestros valores</h2>
                    <p>Creación de Valor Compartido como la forma clave de realizar negocios basados en la Comunicación.</p>
                    <p>Mejora continua hacia la excelencia como forma de trabajar orientada a resultados.</p>
                    <p>Compromiso con una sólida ética laboral de trabajo en equipo, integridad y honestidad.</p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <h2>Objetivos.</h2>
                    <p>Brindar a los clientes atención personalizada y productos de calidad garantizando su fidelidad hacia nosotros y los productos que ofrecemos.</p>
                </div>
            </div>
           

        </div><!-- container -->
    </div><!-- slim-mainpanel -->


    
    <!-- ***** Footer Area Start ***** -->
    <footer class="footer-area section-padding-80-0">
        <div class="container">
            <div class="row justify-content-between">

            </div>
            <div class="row" style="margin-bottom: 30px;">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | UMG - Sololá
            </div>
        </div>
    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- ******* All JS Files ******* -->
    <!-- jQuery js -->
    <script src="{{asset('uza/js/jquery.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('uza/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('uza/js/bootstrap.min.js')}}"></script>
    <!-- All js -->
    <script src="{{asset('uza/js/uza.bundle.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('uza/js/default-assets/active.js')}}"></script>

    <script src="{{asset('libs/chartist/js/chartist.js')}}"></script>
    <script src="{{asset('libs/d3/js/d3.js')}}"></script>
    <script src="{{asset('libs/rickshaw/js/rickshaw.min.js')}}"></script>
    <script src="{{asset('libs/jquery.sparkline.bower/js/jquery.sparkline.min.js')}}"></script>


    <script src="{{asset('js/dashboard.js')}}"></script>

</body>

</html>