<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        
        return view('dashboard.index');
    }

    public function listado()
    {
        
        return view('dashboard.listado');
    }

    public function ventas()
    {
        
        return view('dashboard.ventas');
    }

    public function clientes()
    {
        
        return view('dashboard.clientes');
    }

    public function productos()
    {
        
        return view('dashboard.productos');
    }

}
