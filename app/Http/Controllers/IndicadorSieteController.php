<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Indicador;
use App\Models\Estado;
use App\Models\Ciudad;
use DataTables;
use Validator;
use Carbon;
use Session;
use View;
use DB;

class IndicadorSieteController extends Controller
{
    
    public function index()
    {
        $estados = Estado::all();
        $ciudades = Ciudad::select('ca_state','ca_city')
                        ->whereNotNull('ca_country')
                        ->whereNotNull('ca_state')
                        ->whereNotNull('ca_city')
                        ->where('ca_state','AL')
                        ->groupBy('ca_state', 'ca_city')->get();

        return view('indicadores.clientes-frecuentes-estado', compact('estados','ciudades'));
    }

    public function listado(Request $request){

        $this->validate($request,
            [
                'fh_inicial' => 'required|date_format:"d/m/Y"',
                'fh_final' => 'required|date_format:"d/m/Y"',
                'top_registro' => 'required',
                'ciudad' => 'required',
                'estado' => 'required',
            ],[
                
                'fh_inicial.required' => 'Ingresa una fecha de inicio',
                'fh_inicial.date_format' => 'El formato de la fecha inicial tiene que ser dd/mm/yyyy',
                'fh_final.required' => 'Ingresa una fecha final',
                'fh_final.date_format' => 'El formato de la fecha final tiene que ser dd/mm/yyyy',
                'top_registro.required' => 'Seleccione un ranking de productos',
                'estado.required' => 'Seleccione un estado',
                'ciudad.required' => 'Seleccione una ciudad',

            ]
        );



        $param['fh_inicial'] =self::dbDate($request->fh_inicial);
        $param['fh_final'] =self::dbDate($request->fh_final);
        $param['top'] = self::isNull($request->top_registro);
        $param['estado'] = self::isNull($request->estado);
        $param['ciudad'] = self::isNull($request->ciudad);
        
        
        $param = (object) $param;

        Session::put('filtros',$param);
        Session::save();

        $data = Indicador::getDataIndicadorSiete($param);


        return DataTables::of($data)
                ->addIndexColumn()
                ->make(true);
    }

    public function ciudad(Request $request)
    {

        if ($request->codigo_estado) {
            $id = $request->codigo_estado;
        } else {
            $id = 0;
        }
        

        $estadoSelected = Estado::select('code')->where('code','=',$id)->first();

        if ($estadoSelected) {
            $ciudades = Ciudad::select('ca_state','ca_city')
                        ->whereNotNull('ca_country')
                        ->whereNotNull('ca_state')
                        ->whereNotNull('ca_city')
                        ->where('ca_state',$estadoSelected->code)
                        ->groupBy('ca_state', 'ca_city')->get();

            if ($ciudades) {
                $htmlCiudad = '<select class="form-control" id="ciudad" aria-describedby="ciudad">';
                $htmlCiudad .= '<option value="" >Seleccione un minicipio</option>';
                foreach ($ciudades as $ciudad) {
                    $htmlCiudad .= '<option value="'.$ciudad->ca_city.'">'.$ciudad->ca_city.'</option>';       
                }
                $htmlCiudad .= '</select>';
            } else {                
                $htmlCiudad = '<select class="form-control" id="ciudad" aria-describedby="ciudad">';
                $htmlCiudad .= '<option value="" >Estado no tiene ciudades registrado</option>';
                $htmlCiudad .= '</select>';
            }

        } else {

            $htmlCiudad = '<select class="form-control" id="ciudad" aria-describedby="ciudad">';
            $htmlCiudad .= '<option value="" >Seleccione primero un estado</option>';
            $htmlCiudad .= '</select>';
            
        }

        $ajaxResponse['status'] = 200;
        $ajaxResponse['ciudades'] = $htmlCiudad;
        $ajaxResponse['mensaje'] = 'Consulta de ciudades exitoso';
        return $ajaxResponse;
    }

    static function dbDate($fecha = '', $formatoIngles = true, $simbolo = '-'){        
        if (trim($fecha) == '') {
            return null;
        }

        $fecha = str_replace('/', '-', $fecha);

        $v = Validator::make(['fecha' => $fecha], ['fecha' => 'date']);

        if ($v->fails()) {            
            return null;
        } else {

            if ($formatoIngles) {
                $fecha = date_format(date_create($fecha),'Y-m-d'); 
            } else {
                $fecha = date_format(date_create($fecha),'d-m-Y');
            }

            if ($simbolo != '-') {
                $fecha = str_replace('-', '/', $fecha);
            }
            return $fecha;
        }

    }

    static function isNull($value){

        if (trim($value)=='' || $value == null) {
            return null;
        } else {
            return trim($value);
        }
    }
}
