<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Indicador;
use DataTables;
use Validator;
use Carbon;
use Session;
use View;
use DB;

class IndicadorCuatroController extends Controller
{
    
	public function index()
    {
        $data = array();
        return view('indicadores.comparacion-ventas', compact('data'));
    }

    public function listado(Request $request){

        $this->validate($request,
            [
                'fh_inicial' => 'required|date_format:"d/m/Y"',
                'fh_final' => 'required|date_format:"d/m/Y"'
            ],[
                
                'fh_inicial.required' => 'Ingresa una fecha de inicio',
                'fh_inicial.date_format' => 'El formato de la fecha inicial tiene que ser dd/mm/yyyy',
                'fh_final.required' => 'Ingresa una fecha final',
                'fh_final.date_format' => 'El formato de la fecha final tiene que ser dd/mm/yyyy'

            ]
        );



        $param['fh_inicial'] =self::dbDate($request->fh_inicial);
        $param['fh_final'] =self::dbDate($request->fh_final);
        
        
        $param = (object) $param;

        Session::put('filtros',$param);
        Session::save();

        $data = Indicador::getDataIndicadorCuatro($param);


        return DataTables::of($data)
                ->addIndexColumn()
                ->editColumn('numero_ventas', function ($item) { return number_format($item->numero_ventas, 0, '.', ',');})
                ->make(true);
    }

    static function dbDate($fecha = '', $formatoIngles = true, $simbolo = '-'){        
        if (trim($fecha) == '') {
            return null;
        }

        $fecha = str_replace('/', '-', $fecha);

        $v = Validator::make(['fecha' => $fecha], ['fecha' => 'date']);

        if ($v->fails()) {            
            return null;
        } else {

            if ($formatoIngles) {
                $fecha = date_format(date_create($fecha),'Y-m-d'); 
            } else {
                $fecha = date_format(date_create($fecha),'d-m-Y');
            }

            if ($simbolo != '-') {
                $fecha = str_replace('-', '/', $fecha);
            }
            return $fecha;
        }

    }

    static function isNull($value){

        if (trim($value)=='' || $value == null) {
            return null;
        } else {
            return trim($value);
        }
    }


}