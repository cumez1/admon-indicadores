<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Indicador;
use DataTables;
use Validator;
use Carbon;
use Session;
use View;
use DB;

class IndicadorCatorceController extends Controller
{
  public function index()
    {
        $data = array();
        return view('indicadores.devolucion-productos', compact('data'));
    }

    public function listado(Request $request){

        $this->validate($request,
            [
                'top_registro' => 'required',
            ],[
                
                'top_registro.required' => 'Seleccione un ranking de productos',

            ]
        );

        $param['top'] = self::isNull($request->top_registro);
        
        
        $param = (object) $param;

        Session::put('filtros',$param);
        Session::save();

        $data = Indicador::getDataIndicadorCatorce($param);
        return DataTables::of($data)
                ->addIndexColumn()
                ->make(true);
    }

    static function dbDate($fecha = '', $formatoIngles = true, $simbolo = '-'){        
        if (trim($fecha) == '') {
            return null;
        }

        $fecha = str_replace('/', '-', $fecha);

        $v = Validator::make(['fecha' => $fecha], ['fecha' => 'date']);

        if ($v->fails()) {            
            return null;
        } else {

            if ($formatoIngles) {
                $fecha = date_format(date_create($fecha),'Y-m-d'); 
            } else {
                $fecha = date_format(date_create($fecha),'d-m-Y');
            }

            if ($simbolo != '-') {
                $fecha = str_replace('-', '/', $fecha);
            }
            return $fecha;
        }

    }

    static function isNull($value){

        if (trim($value)=='' || $value == null) {
            return null;
        } else {
            return trim($value);
        }
    }
}
