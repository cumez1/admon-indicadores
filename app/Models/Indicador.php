<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Indicador extends Model
{
    protected $table = 'store_sales';    
    protected $primaryKey = 'id_store';    
    public $timestamps = false;

    protected $connection = 'mysql-utf8';

    public static function getDataIndicadorUno($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;

        $sql  = "CALL sp_producto_mas_vendido($fi,$ff,$topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorDos($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $sql  = "CALL sp_rankin_ventas_por_mes($fi,$ff)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorTres($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;

        $sql  = "CALL sp_rankin_clientes_frecuentes($fi,$ff,$topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorCuatro($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        
        $sql  = "CALL sp_comparaciones_ventas_anio($fi,$ff,$topMax)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorCinco($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;

        $sql  = "CALL sp_ciudad_mayor_consumo($fi,$ff,$topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorSeis($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;

        $sql  = "CALL sp_productos_mas_vendido_estado($fi,$ff,$topMax,$top)";

        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorSiete($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;
        $estado = self::formatString($param->estado);
        $ciudad = self::formatString($param->ciudad);
        #-- CALL sp_clientes_frecuentes_ciudad('2000-01-01','2005-12-31',1000000,100,'MI', 'Springfield');


        $sql  = "CALL sp_clientes_frecuentes_ciudad($fi,$ff,$topMax,$top,$estado,$ciudad)";

        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorOcho($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;


        $sql  = "CALL sp_productos_menos_vendidos($fi,$ff,$topMax,$top)";

        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorNueve($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $top = $param->top;


        $sql  = "CALL sp_stock_productos($fi,$ff,$top)";
    
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorDiez($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;


        $sql  = "CALL sp_empleado_mas_ventas($fi,$ff,$topMax,$top)";

        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorOnce($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;

        $sql  = "CALL sp_producto_mas_vendido($fi,$ff,$topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }



    public static function getDataIndicadorDoce($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
       


        $sql  = "CALL sp_cantidad_producto_vendido_rango_fechas($fi,$ff,$topMax)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }

    public static function getDataIndicadorTrece($param)
    {

        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;


        $sql  = "CALL sp_categoria_mejor_compra($fi,$ff,$topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }



    public static function getDataIndicadorCatorce($param)
    {

        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;


        $sql  = "CALL sp_producto_mas_devuelto($topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;

    }


    public static function getDataIndicadorQuince($param)
    {


        $fi = self::formatString($param->fh_inicial);
        $ff = self::formatString($param->fh_final);
        $topMax = config('constantes.topMaximoConsulta');
        $top = $param->top;

        $sql  = "CALL sp_cantidad_producto_devueltos_rango_fecha($fi,$ff,$topMax,$top)";
        
        $data = collect( DB::select($sql) );


        return $data;
    }

    public static function formatString($value){

        if(is_null($value)){
            
            return "null";
        }else{

            return "'".$value."'";
        }
    }


}
