<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    
    protected $connection = 'mysql';
    protected $table = 'customer_address';    
    protected $primaryKey = 'ca_address_sk';    
    public $timestamps = false;

}
