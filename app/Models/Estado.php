<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'state';    
    protected $primaryKey = 'id_estado';    
    public $timestamps = false;

}
